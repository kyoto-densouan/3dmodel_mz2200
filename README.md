# README #

1/3スケールのSHARP MZ-2200風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- シャープ

## 発売時期
- 1983年7月17日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/MZ-2000#MZ-2200)
- [懐かしのホビーパソコン紹介](https://twitter.com/i/events/797650216383938560)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_mz2200/raw/5f61962330a2b5ce8e116e065521b3f9dec2c6e4/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz2200/raw/5f61962330a2b5ce8e116e065521b3f9dec2c6e4/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz2200/raw/5f61962330a2b5ce8e116e065521b3f9dec2c6e4/ExampleImage.jpg)
